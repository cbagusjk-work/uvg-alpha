#ifndef __LM75B_H
#define __LM75B_H
#ifdef __cplusplus
extern "C" {
#endif
#include "main.h"
#define LM75B_ADDR_TEMP 0x00
#define LM75B_ADDR_CONF 0x01
#define LM75B_ADDR_THYST 0x02
#define LM75B_ADDR_TOS 0x03
#define LM75B_VALUE_THYST1 0x28
#define LM75B_VALUE_THYST2 0x00
#define LM75B_VALUE_TOS1 0x41
#define LM75B_VALUE_TOS2 0x00
#define LM75B_VALUE_CONF_OFF 0x01
#define LM75B_VALUE_CONF_ON 0x00
#define LM75B_Address 0x4F << 1
void LM75B_Init(void);
void LM75B_Shutdown(void);
float LM75B_Read(void);
#ifdef __cplusplus
}
#endif
#endif
