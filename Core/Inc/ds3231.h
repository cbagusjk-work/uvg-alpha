#ifndef __DS3231_H
#define __DS3231_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define DS3231_ADDR 	0x68 << 1
#define DS3231_SEC  	0x00
#define DS3231_MIN		0x01
#define DS3231_HOUR		0x02
#define DS3231_DAY		0x03
#define DS3231_DATE		0x04
#define DS3231_MONTH	0x05
#define DS3231_YEAR		0x06
#define DS3231_A1SEC	0x07
#define DS3231_A1MIN	0x08
#define DS3231_A1HOUR	0x09
#define DS3231_A1DATE	0x0A
#define DS3231_A2MIN	0x0B
#define DS3231_A2HOUR	0x0C
#define DS3231_A2DATE	0x0D
#define DS3231_CR		0x0E
#define DS3231_SR		0x0F
#define DS3231_CR_VAL	0x1F
#define DS3231_SR_VAL	0x00
uint8_t DS3231_Encode(uint8_t Value);
uint8_t DS3231_Get_DOW(uint32_t nYear, uint8_t nMonth, uint8_t nDay);
void DS3231_Write_Register(uint8_t Addr_Ptr, uint8_t Value);
void DS3231_Init(void);
RTC_Data DS3231_Read(void);
void DS3231_Set_Alarm(uint8_t Minutes);
void DS3231_Sync_Time(uint8_t Minutes);
_Bool DS3231_Is_Working_Hour(void);
void DS3231_Set_Date_Time_DOW(Numb_GPS_Data *gData);
#ifdef __cplusplus
}
#endif
#endif
