/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
typedef struct
{
	int8_t	 	RMC_Status;
	float   	Latitude;
	float   	Longitude;
	int16_t 	Speed;
	int16_t 	Direction;
	uint8_t  	RMC_Checksum;
	uint8_t		Time_Hour;
    uint8_t		Time_Min;
    uint8_t		Time_Sec;
	uint8_t		Date_Day;
	uint8_t		Date_Month;
	uint8_t	    Date_Year;
} Numb_GPS_Data;
typedef struct
{
	uint8_t Time_Sec;
	uint8_t Time_Min;
	uint8_t Time_Hour;
	uint8_t DOW;
	uint8_t Date_Day;
	uint8_t Date_Month;
	uint8_t Date_Year;
} RTC_Data;

#include "lm75b.h"
#include "stm32f103rct6.h"
#include "ina219.h"
#include "lsm6ds3.h"
#include "sim5360.h"
#include "ds3231.h"
#include "string.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define REG_EN_Pin GPIO_PIN_15
#define REG_EN_GPIO_Port GPIOC
#define AT_TX2_Pin GPIO_PIN_2
#define AT_TX2_GPIO_Port GPIOA
#define AT_RX2_Pin GPIO_PIN_3
#define AT_RX2_GPIO_Port GPIOA
#define PWRSW_Pin GPIO_PIN_4
#define PWRSW_GPIO_Port GPIOA
#define CLKINT_Pin GPIO_PIN_5
#define CLKINT_GPIO_Port GPIOA
#define CLKINT_EXTI_IRQn EXTI9_5_IRQn
#define IMU_SS_Pin GPIO_PIN_12
#define IMU_SS_GPIO_Port GPIOB
#define IMU_SCK_Pin GPIO_PIN_13
#define IMU_SCK_GPIO_Port GPIOB
#define IMU_MISO_Pin GPIO_PIN_14
#define IMU_MISO_GPIO_Port GPIOB
#define IMU_MOSI_Pin GPIO_PIN_15
#define IMU_MOSI_GPIO_Port GPIOB
#define IMU_IRQ1_Pin GPIO_PIN_6
#define IMU_IRQ1_GPIO_Port GPIOC
#define IMU_IRQ1_EXTI_IRQn EXTI9_5_IRQn
#define IMU_IRQ2_Pin GPIO_PIN_7
#define IMU_IRQ2_GPIO_Port GPIOC
#define IMU_IRQ2_EXTI_IRQn EXTI9_5_IRQn
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_9
#define LED2_GPIO_Port GPIOC
#define GPS_TX5_Pin GPIO_PIN_12
#define GPS_TX5_GPIO_Port GPIOC
#define GPS_RX5_Pin GPIO_PIN_2
#define GPS_RX5_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
