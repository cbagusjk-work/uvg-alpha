#ifndef __INA219_H
#define __INA219_H
#ifdef __cplusplus
extern "C" {
#endif
#include  "main.h"
#define 	INA219_Configuration	0x00
#define 	INA219_Shunt_Voltage	0x01
#define 	INA219_Bus_Voltage		0x02
#define 	INA219_Power					0x03
#define 	INA219_Current		    0x04
#define 	INA219_Calibration		0x05
#define 	INA219_Address  			0x40 << 1
#define 	INA219_vConfiguration	0x0C47
#define 	INA219_Real_vCalib		0x45E7
#define 	INA219_vCalibration		INA219_Real_vCalib
void  		INA219_Init(void);
float   	INA219_Read_Bus_Current(void);
float 		INA219_Read_Shunt_Voltage(void);
float 		INA219_Read_Bus_Voltage(void);
float 		INA219_Read_Bus_Power(void);
#ifdef __cplusplus
}
#endif
#endif
