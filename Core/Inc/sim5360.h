#ifndef __SIM5360_H
#define __SIM5360_H
#ifdef __cplusplus
extern "C" {
#endif
#include "main.h"
void SIM5360A_Init												(void);
void SIM5360A_Check_Power									(void);
void SIM5360A_Echo_Off										(void);
void SIM5360A_Reroute_NMEA_Output					(void);
void SIM5360A_RMC_Selection								(void);
void SIM5360A_Automate_Activating_GPS			(void);
void SIM5360A_Activate_GPS								(void);
void SIM5360A_Check_Network_Registration	(void);
void SIM5360A_Check_PDP_Context						(void);
void SIM5360A_Check_PDS_Status						(void);
void SIM5360A_Open_Network								(void);
void SIM5360A_Open_UDP_Connection					(void);
void SIM5360A_Save_Settings								(void);
void SIM5360A_Power_State_Changer					(_Bool State);
void SIM5360A_Clear_AT_Buffer							(uint8_t Size_Command_Buffer, uint16_t Size_Reply_Buffer);
void SIM5360A_SAR_AT_Command							(const char* AT_Command_Data, uint8_t Size_Reply, uint16_t Timeout);
void SIM5360A_Set_AGPS										(void);
void SIM5360A_Set_LP_Warning							(void);
void SIM5360A_Turn_Off                    (void);
float Degree_Minute_to_Degree							(float* Degree_Minute_Data);
uint8_t SIM5360A_NMEA_Handler							(char NMEA_Char);
#ifdef __cplusplus
}
#endif
#endif
