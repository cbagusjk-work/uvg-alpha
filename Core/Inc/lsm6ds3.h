#ifndef __LSM6DS3_H
#define __LSM6DS3_H
#ifdef __cplusplus
extern "C" {
#endif
#include "main.h"
#define FUNC_CFG_ACCESS		0x01 // Register to Enable/Disable Setting of SM_THS below
#define PEDO_THS_REG		  0x0F // PEDO_4G for XL fsel > 4 g
#define SM_THS				    0x13 // Significant Motion Threshold
#define INT1_CTRL  			  0x0D
#define INT2_CTRL  			  0x0E
#define CTRL1_XL  		  	0x10
#define CTRL4_C  		     	0x13
#define CTRL5_C				    0x14
#define CTRL8_XL  		  	0x17
#define CTRL10_C		    	0x19
#define STATUS_REG		  	0x1E
#define OUTX_L_XL		     	0x28
#define FUNC_SRC		     	0x53
#define TAP_CFG			    	0x58
#define MD1_CFG				    0x5E
#define vFUNC_CFG_ACCESS	0x80 // to enable Setting of SM_THS
#define vFUNC_CFG_ACCESS2 0x00 // to disable Setting of SM_THS
#define vPEDO_THS_REG     0x81 // set PEDO_4G and min threshold
#define vSM_THS				    0x03 // significant motion threshold value 3 step
#define vINT1_CTRL  		  0xC0 // significant motion interrupt at INT1 pin
#define vINT2_CTRL  	   	0x01 // accel data ready at INT2 pin
#define vCTRL1_XL  		   	0x4B // anti-aliasing filter 50 hz, 16 g fsel, 104 hz odr
#define vCTRL4_C  		   	0x0C // all disable except DRDY_MSK and I2C_disable bit
#define vCTRL5_C		    	0x20 // rounding address for accelerometer only
#define vCTRL8_XL  		   	0xC4 // XL LPF2 en
#define vCTRL10_C		     	0x05 // embedded func en (sigmot) (2), sigmot en (0)
#define vTAP_CFG		    	0x60 // pedometer algorithm enable to help sigmot func works properly
#define vMD1_CFG			    0x02 // tilt detection interrupt at INT1
#define lCTRL1_XL         0x00 // Power Down Accelerometer

void LSM6DS3_Init(void);
void LSM6DS3_Read(uint16_t Sensor_Output[]);
void LSM6DS3_Power_Down(void);
uint8_t LSM6DS3_Check_Interrupt(void);
#ifdef __cplusplus
}
#endif
#endif
