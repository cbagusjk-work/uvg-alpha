#ifndef __STM32F103RCT6_H
#define __STM32F103RCT6_H
#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

void STM32F103_Shutdown_LMR62014(void);
void STM32F103_Fetching_Telemetry_Data(uint8_t Method_Format, _Bool Fix_Status);
void STM32F103_NMEA_Parser(char* NMEA_Buffer_Array);
void STM32F103_ReInit_OVERRIDE_Pin(_Bool Pin_State, _Bool Output_State);
void STM32F103_ReInit_CHG_EN_Pin(_Bool Pin_State);
void STM32F103_Pi_Response(uint8_t* Request);
void STM32F103_Send_Telemetry_Data(void);
void STM32F103_Init_All_Sensor(void);
void STM32F103_PVD_Config(void);
void STM32F103_Enter_Standby(void);
uint8_t STM32F103_Check_I2C_Availability(float (*func)(void), float* Output);
_Bool STM32F103_Shutdown_Pi(void);
_Bool STM32F103_Change_Access_Token(void);
_Bool STM32F103_Check_Batt_Volt(void);
_Bool STM32F103_Leap_Year_Check(uint8_t Year);
void STM32F103_Normalize_Date(Numb_GPS_Data* Numb_GPS);
void STM32F103_Set_Internal_Wakeup_Alarm(uint8_t Min, uint8_t Hour);
void STM32F103_Reinit_PA0(_Bool Mode);
void STM32F103_Set_RTC_Date(void);
void STM32F103_Set_RTC_Time(void);
_Bool STM32F103_Is_Working_Hour(void);
void STM32F103_Sleep_Minutes(uint8_t Minutes);
#ifdef __cplusplus
}
#endif
#endif
