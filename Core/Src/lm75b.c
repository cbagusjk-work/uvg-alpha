/* Include header file */
#include 	"lm75b.h"

/* Extern i2c data struct from main */
extern 		I2C_HandleTypeDef hi2c1;

/* Sensor initialization */
void LM75B_Init()
{
    uint8_t Conf[2] = {LM75B_ADDR_CONF, LM75B_VALUE_CONF_ON};
    uint8_t Thyst[3] = {LM75B_ADDR_THYST, LM75B_VALUE_THYST1, LM75B_VALUE_THYST2};
    uint8_t Tos[3] = {LM75B_ADDR_TOS, LM75B_VALUE_TOS1, LM75B_VALUE_TOS2};
    HAL_I2C_Master_Transmit(&hi2c1, LM75B_Address, &Conf[0], 2, 2);
    HAL_I2C_Master_Transmit(&hi2c1, LM75B_Address, &Thyst[0], 3, 3);
    HAL_I2C_Master_Transmit(&hi2c1, LM75B_Address, &Tos[0], 3, 3);
}

/* Read temperature */
float LM75B_Read()
{
	uint8_t 	Temporary_Data[2]   = {0};
	uint16_t 	Decimal_Value		= 0;
	HAL_I2C_Mem_Read(&hi2c1, LM75B_Address, LM75B_ADDR_TEMP, 1, &Temporary_Data[0], 2, 4);
	Decimal_Value                   = (Temporary_Data[0] << 8);
	Decimal_Value                  |=  Temporary_Data[1];
	Decimal_Value                   = (Decimal_Value >> 5);
	return Decimal_Value * 0.125f;
}
