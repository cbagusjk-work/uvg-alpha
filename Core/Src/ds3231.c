/* Include header file */
#include "ds3231.h"

/* Extern i2c data struct from main */
extern I2C_HandleTypeDef hi2c1;

/* Initialization */
void DS3231_Init(void)
{
    DS3231_Write_Register(DS3231_CR, DS3231_CR_VAL);
    DS3231_Write_Register(DS3231_SR, DS3231_SR_VAL);
}

/* Set time, date, and date of week (DOW) */
void DS3231_Set_Date_Time_DOW(Numb_GPS_Data *gData)
{
	DS3231_Write_Register(DS3231_SEC, DS3231_Encode((gData->Time_Sec)));
    DS3231_Write_Register(DS3231_MIN, DS3231_Encode(gData->Time_Min));
    DS3231_Write_Register(DS3231_HOUR, DS3231_Encode(gData->Time_Hour));
    DS3231_Write_Register(DS3231_DAY, DS3231_Get_DOW((gData->Date_Year + 2000), gData->Date_Month, gData->Date_Day));
    DS3231_Write_Register(DS3231_DATE, DS3231_Encode(gData->Date_Day));
    DS3231_Write_Register(DS3231_MONTH, DS3231_Encode(gData->Date_Month));
    DS3231_Write_Register(DS3231_YEAR, DS3231_Encode(gData->Date_Year));
}

/* Calculate DOW */
uint8_t DS3231_Get_DOW(uint32_t nYear, uint8_t nMonth, uint8_t nDay)
{
  uint32_t year = 0U, weekday = 0U;

  year = 2000U + nYear;

  if(nMonth < 3U)
  {
    /*D = { [(23 x month)/9] + day + 4 + year + [(year-1)/4] - [(year-1)/100] + [(year-1)/400] } mod 7*/
    weekday = (((23U * nMonth)/9U) + nDay + 4U + year + ((year-1U)/4U) - ((year-1U)/100U) + ((year-1U)/400U)) % 7U;
  }
  else
  {
    /*D = { [(23 x month)/9] + day + 4 + year + [year/4] - [year/100] + [year/400] - 2 } mod 7*/
    weekday = (((23U * nMonth)/9U) + nDay + 4U + year + (year/4U) - (year/100U) + (year/400U) - 2U ) % 7U;
  }

  return (uint8_t)weekday;
}

/* Encode time and date data according to IC datasheet */
uint8_t DS3231_Encode(uint8_t Value)
{
    uint8_t Encoded = ((Value / 10) << 4) + (Value % 10);
    return Encoded;
}

/* Decode time and date data according to IC datasheet */
uint8_t	DS3231_Decode(uint8_t Value)
{
	uint8_t Decoded = (Value & 15) + (10 * ((Value & (15 << 4)) >> 4));
	return Decoded;
}

/* Set alarm hour and minute match */
void DS3231_Sync_Time(uint8_t Minutes)
{
    RTC_Data kTime;
    kTime = DS3231_Read();
    if((kTime.Time_Min + Minutes) > 59)
    {
        kTime.Time_Min = Minutes - (60 - kTime.Time_Min);
        kTime.Time_Hour++;
    }
		else
		{
			kTime.Time_Min += Minutes;
		}
    if(kTime.Time_Hour > 23)
    {
        kTime.Time_Hour = 0;
    }
    kTime.Time_Sec = 0;
    DS3231_Write_Register(DS3231_A2MIN, DS3231_Encode(kTime.Time_Min));
    DS3231_Write_Register(DS3231_A2HOUR, DS3231_Encode(kTime.Time_Hour));
	DS3231_Write_Register(DS3231_A2DATE, 0x81);
}

/* Write setting to RTC IC */
void DS3231_Write_Register(uint8_t Addr_Ptr, uint8_t Value)
{
    uint8_t Write_Value[2] =
    {
        Addr_Ptr, Value
    };
    HAL_I2C_Master_Transmit(&hi2c1, DS3231_ADDR, &Write_Value[0], 2, 4);
}

/* Read from RTC IC */
RTC_Data DS3231_Read(void)
{
	RTC_Data Time_Date;
	uint8_t RTC_Data_Buffer[7] = {0};
	HAL_I2C_Mem_Read(&hi2c1, DS3231_ADDR, DS3231_SEC, 1, &RTC_Data_Buffer[0], 7, 14);
	Time_Date.Time_Sec = DS3231_Decode(RTC_Data_Buffer[0]);
	Time_Date.Time_Min = DS3231_Decode(RTC_Data_Buffer[1]);
	Time_Date.Time_Hour = DS3231_Decode(RTC_Data_Buffer[2]);
	Time_Date.DOW = DS3231_Decode(RTC_Data_Buffer[3]);
	Time_Date.Date_Day = DS3231_Decode(RTC_Data_Buffer[4]);
	Time_Date.Date_Month = DS3231_Decode(RTC_Data_Buffer[5]);
	Time_Date.Date_Year = DS3231_Decode(RTC_Data_Buffer[6]);
	return Time_Date;
}

/* Set alarm only minute match */
void DS3231_Set_Alarm(uint8_t Minutes)
{
	DS3231_Write_Register(DS3231_A2MIN, DS3231_Encode(Minutes));
    DS3231_Write_Register(DS3231_A2HOUR, 0x80);
	DS3231_Write_Register(DS3231_A2DATE, 0x81);
}

/* Check working hour at current time */
_Bool DS3231_Is_Working_Hour(void)
{
    RTC_Data Current_Time;
    Current_Time = DS3231_Read();
    if(Current_Time.Time_Hour > 5 && Current_Time.Time_Hour < 17)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
