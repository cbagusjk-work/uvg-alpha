/* Include header file */
#include "stm32f103rct6.h"

/* Extern some data struct */
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart2;
extern RTC_HandleTypeDef hrtc;
extern I2C_HandleTypeDef hi2c1;
extern uint8_t GSTM32F103_REG;
extern uint8_t AT_Reply[256];

/* Required variable */
Numb_GPS_Data GPS_Data =
{
	.RMC_Status = 0,
	.Latitude = 0.0f,
	.Longitude = 0.0f,
	.Speed = 0,
	.Direction = 0,
	.RMC_Checksum = 0,
	.Time_Hour = 0,
  .Time_Min = 0,
  .Time_Sec = 0,
	.Date_Day = 0,
	.Date_Month = 0,
	.Date_Year = 0
};
uint8_t Telemetry_Data[150] = {0};
uint8_t Server_IP[15] = "13.229.197.6";

/* NMEA RMC parser */
void STM32F103_NMEA_Parser(char *NMEA_Buffer_Array)
{
	/* Required variable */
	char Temporary_NMEA[15] = {0};
	char *Cursor = &NMEA_Buffer_Array[0];
	int Char_Elapsed = 0;
	uint8_t Commas = 0;
	uint8_t Commas_Counter = 0;
	uint8_t NMEA_Len = strlen(NMEA_Buffer_Array);
	uint8_t NMEA_Self_Checksum  = 0;
	uint8_t Self_Checksum_Str[3] = {0};
	uint8_t NMEA_Checksum[3] = {0};
	struct Temporary_GPS_Data
	{
		float Latitude;
		float Longitude;
		int32_t Date_Stamp;
		int32_t Time_Stamp;
		int16_t Speed;
		int16_t Heading;
	} Temporary_GPS_Data;
  struct GPS_String
  {
      uint8_t Time[6];
      uint8_t Date[6];
      uint8_t Time_Sec[3];
      uint8_t Time_Min[3];
      uint8_t Time_Hour[3];
      uint8_t Date_Day[3];
      uint8_t Date_Month[3];
      uint8_t Date_Year[3];
  } String_GPS;

	/* Copy NMEA checksum that has been present in rx buffer */
	memcpy(&NMEA_Checksum[0], &NMEA_Buffer_Array[NMEA_Len - 2], 2);

	/* Calculate comma at NMEA RMC sentence */
	do
	{
		if(*Cursor == ',')
		{
			Commas_Counter++;
		}
		Cursor++;
	} while(*Cursor != 0x00);

	/* Set pointer to element 1 for checksum (checksum calculation without dollar sign) */
	Cursor = &NMEA_Buffer_Array[1];

	/* Calculate Checksum */
	do
	{
		NMEA_Self_Checksum ^= *Cursor;
		Cursor++;
	} while(*Cursor != '*');

	/* Copy to allocated variable*/
	sprintf((char*) &Self_Checksum_Str[0], "%2X", NMEA_Self_Checksum);

	/* Set pointer to first element */
	Cursor = &NMEA_Buffer_Array[0];

	/* Comma must be 12 and present checksum and self calculated checksum must be same */
	if(Commas_Counter == 12 && strcmp((const char*) &NMEA_Checksum[0], (const char*) &Self_Checksum_Str[0]) == 0)
	{
		if(*Cursor == '$')
		{
			/* Scan max 15 char and limitr comma */
			while (sscanf(Cursor, "%15[^,]%n", &Temporary_NMEA[0], &Char_Elapsed) >= 0)
			{
				/* parse according comma that has been elapsed */
				switch(Commas)
				{
					case 1 :
						Temporary_GPS_Data.Time_Stamp = atoi(Temporary_NMEA);
						if(Temporary_GPS_Data.Time_Stamp <= 235959 && Temporary_GPS_Data.Time_Stamp >= 0)
						{
							memcpy(&String_GPS.Time[0], Temporary_NMEA, 6);
						}
						break;
					case 2 :
						if(Temporary_NMEA[0] == 'A')
						{
							GPS_Data.RMC_Status = 1;
						}
						else
						{
							GPS_Data.RMC_Status = 0;
						}
						break;
					case 3 :
						Temporary_GPS_Data.Latitude = atof(Temporary_NMEA);
						Temporary_GPS_Data.Latitude = Degree_Minute_to_Degree(&Temporary_GPS_Data.Latitude);
						if(Temporary_GPS_Data.Latitude <= 12.0f && Temporary_GPS_Data.Latitude >= 5.0f)
						{
							GPS_Data.Latitude = Temporary_GPS_Data.Latitude;
						}
						break;
					case 4 :
						if(Temporary_NMEA[0] == 'S')
						{
							GPS_Data.Latitude *= (-1.0f);
						}
						else if(Temporary_NMEA[0] == 'N')
						{
							GPS_Data.Latitude *= 1.0f;
						}
						break;
					case 5 :
						Temporary_GPS_Data.Longitude = atof(Temporary_NMEA);
						Temporary_GPS_Data.Longitude = Degree_Minute_to_Degree(&Temporary_GPS_Data.Longitude);
						if(Temporary_GPS_Data.Longitude <= 141.0f && Temporary_GPS_Data.Longitude >= 95.0f)
						{
							GPS_Data.Longitude = Temporary_GPS_Data.Longitude;
						}
						break;
					case 6 :
						if(Temporary_NMEA[0] == 'W')
						{
							GPS_Data.Longitude *= (-1.0f);
						}
						else if(Temporary_NMEA[0] == 'E')
						{
							GPS_Data.Longitude *= 1.0f;
						}
						break;
					case 7 :
						/* Convert to km/h */
						Temporary_GPS_Data.Speed = (uint16_t)((atof(Temporary_NMEA))*1.852);
						if(Temporary_GPS_Data.Speed <= 300 && Temporary_GPS_Data.Speed >= 0)
						{
							GPS_Data.Speed = Temporary_GPS_Data.Speed;
						}
						break;
					case 8 :
						Temporary_GPS_Data.Heading = (uint16_t) atof(Temporary_NMEA);
						if(Temporary_GPS_Data.Heading <= 360 && Temporary_GPS_Data.Heading >= 0)
						{
							GPS_Data.Direction = Temporary_GPS_Data.Heading;
						}
						break;
					case 9 :
						Temporary_GPS_Data.Date_Stamp = atoi(Temporary_NMEA);
						if(Temporary_GPS_Data.Date_Stamp <= 311299 && Temporary_GPS_Data.Date_Stamp >= 0)
						{
							memcpy(&String_GPS.Date[0], Temporary_NMEA, 6);
              memcpy(&String_GPS.Date_Day[0], &String_GPS.Date[0], 2);
              memcpy(&String_GPS.Date_Month[0], &String_GPS.Date[2], 2);
              memcpy(&String_GPS.Date_Year[0], &String_GPS.Date[4], 2);
              memcpy(&String_GPS.Time_Hour[0], &String_GPS.Time[0], 2);
              memcpy(&String_GPS.Time_Min[0], &String_GPS.Time[2], 2);
              memcpy(&String_GPS.Time_Sec[0], &String_GPS.Time[4], 2);
              GPS_Data.Date_Day = atoi((const char*) &String_GPS.Date_Day[0]);
              GPS_Data.Date_Month = atoi((const char*) &String_GPS.Date_Month[0]);
              GPS_Data.Date_Year = atoi((const char*) &String_GPS.Date_Year[0]);
              GPS_Data.Time_Hour = atoi((const char*) &String_GPS.Time_Hour[0]);
              GPS_Data.Time_Min = atoi((const char*) &String_GPS.Time_Min[0]);
              GPS_Data.Time_Sec = atoi((const char*) &String_GPS.Time_Sec[0]);

							/* Change to GMT +7 time */
              if(GPS_Data.Time_Hour >= 17)
              {
                  GPS_Data.Time_Hour -= 17;
                  STM32F103_Normalize_Date(&GPS_Data);
              }
              else
              {
                  GPS_Data.Time_Hour += 7;
              }

							/* RTC not synchronize yet */
              if(!(GSTM32F103_REG & 0x10))
              {
                DS3231_Set_Date_Time_DOW(&GPS_Data);
                STM32F103_Set_RTC_Time();
                STM32F103_Set_RTC_Date();
                DS3231_Sync_Time(5);
                __HAL_RCC_PWR_CLK_ENABLE();
                __HAL_RCC_BKP_CLK_ENABLE();
                HAL_PWR_EnableBkUpAccess();

								/* Set bit 0 in Backup Register DR1 */
                SET_BIT(BKP->DR1, 0x00000001);
                GSTM32F103_REG |= 0x10;
                if(DS3231_Is_Working_Hour())
                {
                  STM32F103_Set_Internal_Wakeup_Alarm(0,17);
                }
                else
                {
                  STM32F103_Set_Internal_Wakeup_Alarm(0,6);
                  STM32F103_Enter_Standby();
                }
              }
						}
						break;
					case 12:
						GPS_Data.RMC_Checksum  = strtol(Temporary_NMEA, NULL, 16);
						break;
					default:
						break;
				}
				/* Move cursor foward according to elapsed char (max 15 chars) */
				Cursor += Char_Elapsed;

				/* Check for comma */
				do
				{
					Cursor ++;
					Commas ++;
				} while(*Cursor == ',');

				/* End of string */
				if(*Cursor == 0x00) break;

				/* Comma more than 11 */
				if(Commas > 11)
				{
					Cursor += 2;
				}
			}
		}
	}
}
void STM32F103_Set_RTC_Date(void)
{
  RTC_DateTypeDef cDate;
  cDate.Date = GPS_Data.Date_Day;
  cDate.Month = GPS_Data.Date_Month;
  cDate.Year = GPS_Data.Date_Year;
  cDate.WeekDay = RTC_WEEKDAY_MONDAY;
  HAL_RTC_SetDate(&hrtc, &cDate, RTC_FORMAT_BIN);
}
void STM32F103_Set_RTC_Time(void)
{
  RTC_TimeTypeDef cTime;
  cTime.Seconds = GPS_Data.Time_Sec;
  cTime.Minutes = GPS_Data.Time_Min;
  cTime.Hours = GPS_Data.Time_Hour;
  HAL_RTC_SetTime(&hrtc, &cTime, RTC_FORMAT_BIN);
}
_Bool STM32F103_Is_Working_Hour(void)
{
  RTC_TimeTypeDef aTime;
  HAL_RTC_GetTime(&hrtc, &aTime, RTC_FORMAT_BIN);
  if(aTime.Hours > 5 && aTime.Hours < 17)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}
void STM32F103_Sleep_Minutes(uint8_t Minutes)
{
  RTC_TimeTypeDef aTime;
  RTC_AlarmTypeDef aAlarm;
  HAL_RTC_GetTime(&hrtc, &aTime, RTC_FORMAT_BIN);
	HAL_RTC_SetTime(&hrtc, &aTime, RTC_FORMAT_BIN);
  if((aTime.Minutes + Minutes) > 59)
  {
    aTime.Minutes = Minutes - (60 - aTime.Minutes);
    aTime.Hours++;
  }
	else
	{
		aTime.Minutes += Minutes;
	}
  if(aTime.Hours > 23)
  {
    aTime.Hours = 0;
  }
  aTime.Seconds = 0;
  aAlarm.AlarmTime = aTime;
  aAlarm.Alarm = RTC_ALARM_A;
  HAL_RTC_SetAlarm_IT(&hrtc, &aAlarm, RTC_FORMAT_BIN);
}
void STM32F103_Fetching_Telemetry_Data(uint8_t Method_Format, _Bool Fix_Status)
{
	/* UVG Alpha Thingsboard Token */
  // W5fEO1RRP7WU0utucn9G UVG #2
  // IkV9bpKpeXHO7OuJnLx7 UVG #3
  // 8DHPnrOuvAK4iaSQSCED UVG #4
  // KfBuli3y6VpN5TI8suLG UVG #6
  // t1iCAE7atqRfqkqGKmMB UVG #7
  // MWb9sgzWBhgtpD0mxDXw	UVG #8
  // ByZjf5cNJadaXW2Rk6OG UVG #9
  // 23ZPO5aiktAfaTy9SfAR UVG #10

	/* Required variable */
	uint8_t Device_Token[21] = "KfBuli3y6VpN5TI8suLG\0";
	uint8_t CSQ_String[6] = {0};
	uint8_t	GPS_Fix_Flag = Fix_Status;
  uint8_t Tel_Len = 0;
	uint16_t Message_ID_Tempo = 0;
  uint16_t Battery_Voltage = 0;
	int16_t	Power_Consumption = 0;
	uint32_t Seed_Rand = 0;
	uint32_t Token_Tempo = 0;
	float Temperature = 0.0f;
	float CSQ_Numb = 0.0f;
	char* CSQ_Ptr;

	/* Generate 16 bit Message ID */
	do
	{
		Seed_Rand = HAL_GetTick();
		srand(Seed_Rand);
		Message_ID_Tempo = rand() % 65535 + 10000;
    Telemetry_Data[2] = (uint8_t) (Message_ID_Tempo >> 8);
    Telemetry_Data[3] = (uint8_t) Message_ID_Tempo;
	} while(Telemetry_Data[2] == 0x00 || Telemetry_Data[3] == 0x00);

	/* Generate 32 bit Token */
	do
	{
		Seed_Rand = HAL_GetTick();
		srand(Seed_Rand);
		Token_Tempo	= rand() % 4294967295 + 1000000000;
		Telemetry_Data[4] = (uint8_t) (Token_Tempo>>24);
		Telemetry_Data[5] = (uint8_t) (Token_Tempo>>16);
		Telemetry_Data[6] = (uint8_t) (Token_Tempo>>8);
		Telemetry_Data[7] = (uint8_t)  Token_Tempo;
	} while(Telemetry_Data[4] == 0x00 || Telemetry_Data[5] == 0x00 || Telemetry_Data[6] == 0x00 || Telemetry_Data[7] == 0x00);

	/* Version, Type, and Token Length */
	memset((void*) &Telemetry_Data[0], 0x54, 1);

	/* coAP Code (Post = 0x02) */
	memset((void*) &Telemetry_Data[1], 0x02, 1);

	/* Option Delta details available at coAP RFC */
	/* Option Delta #1, Type = URI_Path, length 3 bytes */
	memset((void*) &Telemetry_Data[8], 0xB3, 1);
	memcpy(&Telemetry_Data[9], "api", 3);

	/* Option Delta #2, Type = reserved, length 2 bytes */
	memset((void*) &Telemetry_Data[12], 0x02, 1);
	memcpy(&Telemetry_Data[13], "v1", 2);

	/* Option Delta #3, Type = reserved, length 20 bytes */
	memset((void*) &Telemetry_Data[15], 0x0D, 1);
	memset((void*) &Telemetry_Data[16], 0x07, 1);
	memcpy(&Telemetry_Data[17], &Device_Token[0], 20);

	/* Option Delta #4, Type = reserved, length 9 bytes */
	memset((void*) &Telemetry_Data[37], 0x09, 1);
	memcpy(&Telemetry_Data[38], "telemetry", 9);

	/* End of Option Marker */
	memset((void*) &Telemetry_Data[47], 0xFF, 1);

	/* First section for payload (JSON) */
	memset((void*) &Telemetry_Data[48], 0x7B, 1);

	/* Clear remaining 100 bytes */
	memset((void*) &Telemetry_Data[49], 0x00, 100);

	/* Get some data from sensor */
  Temperature	= LM75B_Read();
  Battery_Voltage	= (uint16_t) INA219_Read_Bus_Voltage();
  Power_Consumption =  Battery_Voltage * INA219_Read_Bus_Current();

	/* Get signal quality */
  SIM5360A_Clear_AT_Buffer(64,256);
  SIM5360A_SAR_AT_Command("AT+CSQ\r\n\0", 21, 50);

	/* Is digit in the first data of signal quality */
  if(AT_Reply[8] >= 0x30 && AT_Reply[8] <= 0x39)
  {
      memcpy(&CSQ_String[0], &AT_Reply[8], 5);
  }
  else if(AT_Reply[9] >= 0x30 && AT_Reply[9] <= 0x39)
  {
      memcpy(&CSQ_String[0], &AT_Reply[9], 5);
  }
  else
  {
      memcpy(&CSQ_String[0], "00.00", 5);
  }

	/* Search for comma and replace with dot to make float data type for ease fetching with others data */
  CSQ_Ptr = strstr((char*) &CSQ_String[0], ",");
  if(CSQ_Ptr != NULL)
  {
      *CSQ_Ptr = '.';
  }

	/* Carriage return replace with zero */
  if(CSQ_String[4] == 0x0D)
  {
      CSQ_String[4] = 0x30;
  }

	/* Convert to float */
  CSQ_Numb = atof((const char*) &CSQ_String[0]);

	/* Fetch GPS data */
  if(GPS_Data.Latitude != 0 && GPS_Fix_Flag)
  {
		/* Calculate current telemetry data length */
    Tel_Len = strlen((const char*) &Telemetry_Data[0]);

		/* Copy "1": to telemetry data buffer */
    memcpy(&Telemetry_Data[Tel_Len], "\"1\":", 4);

		/* Copy 10 digit float GPS latitude */
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%10.7f", GPS_Data.Latitude);

		/* Add comma */
    memset((void*) &Telemetry_Data[Tel_Len + 14], 0x2C, 1);

		/* Set NULL or 0 for 5 chars afterwards to make sure that further strlen function calculate data len correctly */
    memset((void*) &Telemetry_Data[Tel_Len + 15], 0x00, 5);
  }
  if(GPS_Data.Longitude != 0 && GPS_Fix_Flag)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"2\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%10.7f", GPS_Data.Longitude);
    memset((void*) &Telemetry_Data[Tel_Len + 14], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 15], 0x00, 5);
  }
  if(GPS_Data.Direction != 0 && GPS_Fix_Flag)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"3\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%3d", GPS_Data.Direction);
    memset((void*) &Telemetry_Data[Tel_Len + 7], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 8], 0x00, 5);
  }
  if(GPS_Data.Speed != 0 && GPS_Fix_Flag)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"4\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%3d", GPS_Data.Speed);
    memset((void*) &Telemetry_Data[Tel_Len + 7], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 8], 0x00, 5);
  }
  if(Temperature != 0)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"5\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%6.3f", Temperature);
    memset((void*) &Telemetry_Data[Tel_Len + 10], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 11], 0x00, 5);
  }
  if(Battery_Voltage != 0)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"6\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%4d", Battery_Voltage);
    memset((void*) &Telemetry_Data[Tel_Len + 8], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 9], 0x00, 5);
  }
  if(Power_Consumption != 0)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"7\":", 4);
    sprintf((char*) &Telemetry_Data[Tel_Len + 4], "%4d", Power_Consumption);
    memset((void*) &Telemetry_Data[Tel_Len + 8], 0x2C, 1);
    memset((void*) &Telemetry_Data[Tel_Len + 9], 0x00, 5);
  }
  if(CSQ_Numb <= 99.99f)
  {
    Tel_Len = strlen((const char*)&Telemetry_Data[0]);
    memcpy(&Telemetry_Data[Tel_Len], "\"8\":", 4);
    memcpy(&Telemetry_Data[Tel_Len + 4], &CSQ_String[0], 5);
    memset((void*) &Telemetry_Data[Tel_Len + 10], 0x00, 5);
  }

	/* Add closed curly bracket and CR LN */
  Tel_Len = strlen((const char*)&Telemetry_Data[0]);
  memcpy((void*)&Telemetry_Data[Tel_Len], "}", 1);
  memcpy((void*)&Telemetry_Data[Tel_Len + 1], "\r\n", 2);
}

void STM32F103_Send_Telemetry_Data(void)
{
	uint8_t Send_Command[42] = {0};
	strcpy((char*) &Send_Command[0], "AT+CIPSEND=0,");
	sprintf((char*) &Send_Command[13], "%3d", strlen((const char*)&Telemetry_Data[0]));
	strcpy((char*) &Send_Command[16], ",\"");
	strcpy((char*) &Send_Command[18],(char*) Server_IP);
	strcpy((char*) &Send_Command[strlen((const char*) Send_Command)], "\",5683\r\n");
	SIM5360A_SAR_AT_Command((const char*) &Send_Command[0], 100, 300);
	if(strstr((char*) &AT_Reply[0], ">") != NULL)
	{
		HAL_UART_Transmit(&huart2, &Telemetry_Data[0], strlen((char*) Telemetry_Data), 150);
	}
}
float Degree_Minute_to_Degree(float* Degree_Minute_Data)
{
	float Separate_Degree 	= *Degree_Minute_Data / 100.0f;
	float Degree      			= (uint8_t) Separate_Degree;
	float Minute_to_Degree  = (Separate_Degree - Degree) / 0.60f;
	Degree           	   	 += Minute_to_Degree;
	return Degree;
}
_Bool STM32F103_Check_Batt_Volt(void)
{
	float Batt_Volt = 0;
	uint8_t Power_Control_Register = (uint8_t) READ_BIT(PWR->CSR, PWR_CSR_WUF);
	for(uint8_t Index = 0; Index < 10; Index++)
	{
		Batt_Volt += INA219_Read_Bus_Voltage();
		HAL_Delay(10);
	}
	Batt_Volt /= 10;

	/* Lower the battery voltage to give buffer to charge (also for battery health to not too low voltage) */
	if(Power_Control_Register & (uint8_t) PWR_CSR_WUF)
	{
		Batt_Volt -= 200;
	}
	else
	{
		Batt_Volt -= 50;
	}
	if(Batt_Volt > 3500)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
void STM32F103_PVD_Config(void)
{
	PWR_PVDTypeDef PVD_Config = {0};
	PVD_Config.Mode = PWR_PVD_MODE_IT_RISING;
	PVD_Config.PVDLevel = PWR_PVDLEVEL_2;
	HAL_PWR_ConfigPVD(&PVD_Config);
	HAL_PWR_EnablePVD();
}
void STM32F103_Enter_Standby(void)
{
	SIM5360A_Turn_Off();
	HAL_GPIO_WritePin(GPIOC, LED1_Pin|LED2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(REG_EN_GPIO_Port, REG_EN_Pin, GPIO_PIN_RESET);
    SET_BIT(PWR->CR, PWR_CR_CWUF);
	HAL_PWR_EnterSTANDBYMode();
}
_Bool STM32F103_Leap_Year_Check(uint8_t Year)
{
    uint16_t TYear = Year + 2000;
    if((TYear % 4) == 0)
    {
        if((TYear % 100) == 0)
        {
            if((TYear % 400) == 0) return 1;
            else return 0;
        }
        else return 1;
    }
    else return 0;
}

/* Determine date when GPS Time (GMT +0) has been converted to GMT +7 and that is elapsed current date time (more than 12 am), so we determine that it is just next day or maybe next month */
void STM32F103_Normalize_Date(Numb_GPS_Data* Numb_GPS)
{
    if((Numb_GPS->Date_Month == 1U) || (Numb_GPS->Date_Month == 3U) || (Numb_GPS->Date_Month == 5U) || (Numb_GPS->Date_Month == 7U) || \
    (Numb_GPS->Date_Month == 8U) || (Numb_GPS->Date_Month == 10U) || (Numb_GPS->Date_Month == 12U))
    {
        if(Numb_GPS->Date_Day < 31U)
        {
            Numb_GPS->Date_Day++;
        }
        else
        {
            if(Numb_GPS->Date_Month != 12U)
            {
                Numb_GPS->Date_Month++;
                Numb_GPS->Date_Day = 1U;
            }
            else
            {
                Numb_GPS->Date_Month = 1U;
                Numb_GPS->Date_Day = 1U;
                Numb_GPS->Date_Year++;
            }
        }
    }
    else if((Numb_GPS->Date_Month == 4U) || (Numb_GPS->Date_Month == 6U) || (Numb_GPS->Date_Month == 9U) || (Numb_GPS->Date_Month == 11U))
    {
        if(Numb_GPS->Date_Day < 30U)
        {
            Numb_GPS->Date_Day++;
        }
        else
        {
            Numb_GPS->Date_Month++;
            Numb_GPS->Date_Day = 1U;
        }
    }
    else if(Numb_GPS->Date_Month == 2U)
    {
        if(Numb_GPS->Date_Day < 28U)
        {
            Numb_GPS->Date_Day++;
        }
        else if(Numb_GPS->Date_Day == 28U)
        {
            if(STM32F103_Leap_Year_Check(Numb_GPS->Date_Year))
            {
                Numb_GPS->Date_Day++;
            }
            else
            {
                Numb_GPS->Date_Month++;
                Numb_GPS->Date_Day = 1U;
            }
        }
        else if(Numb_GPS->Date_Day == 29U)
        {
            Numb_GPS->Date_Month++;
            Numb_GPS->Date_Day = 1U;
        }
    }
}
void STM32F103_Set_Internal_Wakeup_Alarm(uint8_t Min, uint8_t Hour)
{
    RTC_TimeTypeDef wTime;
    RTC_AlarmTypeDef wAlarm;
    wTime.Hours = Hour;
	wTime.Minutes = Min;
	wTime.Seconds = 0;
	wAlarm.AlarmTime = wTime;
	wAlarm.Alarm = RTC_ALARM_A;
	HAL_RTC_SetAlarm_IT(&hrtc, &wAlarm, RTC_FORMAT_BIN);
}
