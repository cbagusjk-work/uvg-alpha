/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart5;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
/* -, -, PVD, DTRD, Tilt, Sigmot, Sigmot/Tilt, RTC Internal */
uint8_t Interrupt_Flag = 0;
/* -, -, -, Activate GPS, Auto On GPS, RMC Selection, Reroute NMEA, Check Power */
uint8_t SIM5360_Init_Error_Flag = 0;
/* -, -, -, DS3231 Sync, NMEA_Status, NMEA_Status, Message Format, VCIG Detect */
uint8_t GSTM32F103_REG = 0;
/* LSM6DS3 Private Variable */
uint16_t Accel_Value[3] = {0};
/* SIM5360A Private Variable */
uint8_t SIM5360A_NMEA_Char = 0;
uint64_t RTC_Read = 0;
uint64_t Time_Send = 0;
uint64_t Check_Connection = 0;
uint64_t Check_Battery_Volt = 0;

float LM75 = 0.0f;
uint8_t DR1 = 0;
uint8_t NMEA_State = 0;
RTC_Data RTC_Buffer;
RTC_TimeTypeDef nTime;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI2_Init(void);
static void MX_UART5_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_RTC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */


  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */

  /* USER CODE BEGIN 2 */
  /* Initialize all GPIO */
	MX_GPIO_Init();

  /* Initialize DMA periph */
  MX_DMA_Init();

  /* Wait 10 miliseconds */
	HAL_Delay(10);

  /* Enable clock for PWR and BKP section of MCU to access RTC Backup Register */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_RCC_BKP_CLK_ENABLE();
	HAL_PWR_EnableBkUpAccess();

  /* Read Backup Register */
	DR1 = (uint8_t) READ_REG(BKP->DR1);

  /* Bit 0 is set, RTC has been synchronize */
	if(DR1 & 0x01)
	{
    /* Set bit at global variable to indicate RTC has been synced */
    GSTM32F103_REG |= 0x10;
	}

  /* Initialize I2C periph */
  MX_I2C1_Init();

  /* Initialize internal RTC periph, not accurate because not using external crystal (pin being used by another feature) */
	MX_RTC_Init();

  /* Initialize external RTC IC */
  DS3231_Init();

  /* Low supply voltage detected, 5th bit is set by interrupt service routine */
	if(Interrupt_Flag & 0x20)
	{
    /* Go to sleep mode and wake up at particular time depending on current time */
    if((GSTM32F103_REG & 0x10) && !STM32F103_Is_Working_Hour())
    {
      STM32F103_Set_Internal_Wakeup_Alarm(0,6);
      STM32F103_Enter_Standby();
    }
    else
    {
      STM32F103_Sleep_Minutes(10);
      STM32F103_Enter_Standby();
    }
	}

  /* Turn on LED */
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);

  /* Configure Programmable Voltage Detector in case this is the first time running */
	STM32F103_PVD_Config();

  /* Initialize power monitoring sensor */
	INA219_Init();

  /* Delay 1 miliseconds */
	HAL_Delay(1);

  /* Check battery voltage, this is similar to PVD but in higher measurement and more flexible, PVD just 1 threshold voltage */
	if(!STM32F103_Check_Batt_Volt())
	{
    /* Go to sleep mode and wake up at particular time depending on current time */
    if((GSTM32F103_REG & 0x10) && !DS3231_Is_Working_Hour())
    {
      STM32F103_Set_Internal_Wakeup_Alarm(0,6);
      STM32F103_Enter_Standby();
    }
    else
    {
      STM32F103_Sleep_Minutes(1);
      STM32F103_Enter_Standby();
    }
	}

  /* Synchronize internal RTC */
	if(GSTM32F103_REG & 0x10)
	{
        RTC_Buffer = DS3231_Read();
        nTime.Hours = RTC_Buffer.Time_Hour;
        nTime.Minutes = RTC_Buffer.Time_Min;
        nTime.Seconds = RTC_Buffer.Time_Sec;
        HAL_RTC_SetTime(&hrtc, &nTime, RTC_FORMAT_BIN);
        DS3231_Sync_Time(1);
        if(DS3231_Is_Working_Hour())
        {
            STM32F103_Set_Internal_Wakeup_Alarm(0,17);
        }
        else
        {
            STM32F103_Set_Internal_Wakeup_Alarm(0,6);
            STM32F103_Enter_Standby();
        }
	}

  /* Initialize SPI2 periph*/
	MX_SPI2_Init();

  /* Initialize UART */
	MX_UART5_Init();
	MX_USART2_UART_Init();

  /* Turn off LED */
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

  /* Initialize temperature sensor */
	LM75B_Init();

  /* Initialize GSM modem and GPS receiver */
	SIM5360A_Init();

  /* Enable UART receive interrupt */
	__HAL_UART_ENABLE_IT(&huart5, UART_IT_RXNE);


	RTC_Read = HAL_GetTick();
	Time_Send = RTC_Read;
	Check_Connection = RTC_Read;
	Check_Battery_Volt = RTC_Read;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
			if((HAL_GetTick() - RTC_Read) > 999)
			{
          RTC_Buffer = DS3231_Read();
          HAL_RTC_GetTime(&hrtc, &nTime, RTC_FORMAT_BIN);
					RTC_Read = HAL_GetTick();
			}

      /* Error occurred in GSM modem or GPS receiver */
			if(SIM5360_Init_Error_Flag != 0x00)
			{
        /* Reconfigure specific section that has error */
				if(SIM5360_Init_Error_Flag & 0x04)
				{
						SIM5360A_RMC_Selection();
				}
				if(SIM5360_Init_Error_Flag & 0x08)
				{
						SIM5360A_Automate_Activating_GPS();
				}
				if(SIM5360_Init_Error_Flag & 0x10)
				{
						SIM5360A_Activate_GPS();
				}
			}

      /* Alarm from internal RTC */
			if(Interrupt_Flag & 0x01)
			{
        /* Clear flag */
				Interrupt_Flag &= ~(0x01);

        /* Check working hour of current time and initiate sleep when out of working hour */
				if(STM32F103_Is_Working_Hour())
				{
					STM32F103_Set_Internal_Wakeup_Alarm(0,17);
				}
				else
				{
					STM32F103_Set_Internal_Wakeup_Alarm(0,6);
					STM32F103_Enter_Standby();
				}
			}

      /* Interrupt from IMU */
			if(Interrupt_Flag & 0x02)
			{
        /* Clear flag */
					Interrupt_Flag &= ~(0x02);

          /* Check specific interrupt source */
					Interrupt_Flag |= LSM6DS3_Check_Interrupt();

          /* 0x04 = significant motion detected, 0x08 = tilt detected, 0x02 = unknown */
					if(Interrupt_Flag & 0x04)
					{
							Interrupt_Flag &= ~(0x04);
					}
					if(Interrupt_Flag & 0x08)
					{
							Interrupt_Flag &= ~(0x08);
					}
					if(Interrupt_Flag & 0x02)
					{
							Interrupt_Flag &= ~(0x02);
					}
			}

      /* Accelerometer data ready */
			if(Interrupt_Flag & 0x10)
			{
					Interrupt_Flag &= ~(0x10);
					LSM6DS3_Read(Accel_Value);
			}

      /* Interrupt from external RTC */
			if(Interrupt_Flag & 0x20)
			{
          Interrupt_Flag &= ~(0x20);

          /* Clear external RTC interrupt flag (at external RTC IC) */
          DS3231_Write_Register(DS3231_SR, 0x00);
          RTC_Buffer = DS3231_Read();
          nTime.Hours = RTC_Buffer.Time_Hour;
          nTime.Minutes = RTC_Buffer.Time_Min;
          nTime.Seconds = RTC_Buffer.Time_Sec;
          HAL_RTC_SetTime(&hrtc, &nTime, RTC_FORMAT_BIN);

          /* Next sync 5 minutes from now */
          DS3231_Sync_Time(5);
			}

      /* Reserved */
			if(Interrupt_Flag & 0x40)
			{
					Interrupt_Flag &= ~(0x40);
			}

      /* Char from UART has been received */
			if(Interrupt_Flag & 0x80)
			{
        /* Clear flag */
				Interrupt_Flag &= ~(0x80);

        /* Store NMEA */
				NMEA_State = SIM5360A_NMEA_Handler((char) SIM5360A_NMEA_Char);

        /* Full sentence of NMEA has been received */
				if(NMEA_State == 1)
				{
						GSTM32F103_REG |= 0x04;
				}
				else
				{
						GSTM32F103_REG &= ~(0x04);
				}

        /* NMEA_state 0 and 1 indicate that full NMEA sentence but has GPS FIX (1) or not (0) */
				if(NMEA_State == 0 || NMEA_State == 1)
				{
          /* Limit time to send every 8 seconds */
					if((HAL_GetTick() - Time_Send) > 8000)
					{
            /* Disable UART rx interrupt to prevent distuption */
						__HAL_UART_DISABLE_IT(&huart5, UART_IT_RXNE);

            /* Create COAP message format */
						STM32F103_Fetching_Telemetry_Data((GSTM32F103_REG & 0x02), (GSTM32F103_REG & 0x04));

						if(NMEA_State)
						{
								HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
						}
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
						STM32F103_Send_Telemetry_Data();
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
						HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
						__HAL_UART_ENABLE_IT(&huart5, UART_IT_RXNE);
						Time_Send = HAL_GetTick();
					}
				}
			}

      /* Check connection and battery every 10 seconds */
			if((HAL_GetTick() - Check_Connection) > 10000)
			{
					if(!STM32F103_Check_Batt_Volt())
					{
							STM32F103_Sleep_Minutes(1);;
							STM32F103_Enter_Standby();
					}
          SIM5360A_Check_Network_Registration();
					SIM5360A_Open_UDP_Connection();
          SIM5360A_SAR_AT_Command("AT+CPING=\"www.google.com\",1,2,4,1000,10000,255\r\n\0",4,50);
					Check_Connection = HAL_GetTick();
			}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef DateToUpdate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
  hrtc.Init.OutPut = RTC_OUTPUTSOURCE_ALARM;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
	if(!(GSTM32F103_REG & 0x10))
	{
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0;
  sTime.Minutes = 0;
  sTime.Seconds = 0;

  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  DateToUpdate.WeekDay = RTC_WEEKDAY_MONDAY;
  DateToUpdate.Month = RTC_MONTH_JANUARY;
  DateToUpdate.Date = 1;
  DateToUpdate.Year = 0;

  if (HAL_RTC_SetDate(&hrtc, &DateToUpdate, RTC_FORMAT_BIN) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */
	}
  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief UART5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART5_Init(void)
{

  /* USER CODE BEGIN UART5_Init 0 */

  /* USER CODE END UART5_Init 0 */

  /* USER CODE BEGIN UART5_Init 1 */

  /* USER CODE END UART5_Init 1 */
  huart5.Instance = UART5;
  huart5.Init.BaudRate = 115200;
  huart5.Init.WordLength = UART_WORDLENGTH_8B;
  huart5.Init.StopBits = UART_STOPBITS_1;
  huart5.Init.Parity = UART_PARITY_NONE;
  huart5.Init.Mode = UART_MODE_RX;
  huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart5.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart5) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART5_Init 2 */

  /* USER CODE END UART5_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(REG_EN_GPIO_Port, REG_EN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PWRSW_GPIO_Port, PWRSW_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED1_Pin|LED2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC13 PC14 PC0 PC1
                           PC2 PC3 PC4 PC5
                           PC10 PC11 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_0|GPIO_PIN_1
                          |GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : REG_EN_Pin */
  GPIO_InitStruct.Pin = REG_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(REG_EN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA6 PA7
                           PA8 PA9 PA10 PA11
                           PA12 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_6|GPIO_PIN_7
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_12|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PWRSW_Pin */
  GPIO_InitStruct.Pin = PWRSW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PWRSW_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CLKINT_Pin */
  GPIO_InitStruct.Pin = CLKINT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CLKINT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 PB2 PB10
                           PB11 PB3 PB4 PB5
                           PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : IMU_SS_Pin */
  GPIO_InitStruct.Pin = IMU_SS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(IMU_SS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : IMU_IRQ1_Pin IMU_IRQ2_Pin */
  GPIO_InitStruct.Pin = IMU_IRQ1_Pin|IMU_IRQ2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LED1_Pin LED2_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
