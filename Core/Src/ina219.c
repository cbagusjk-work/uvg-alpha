/* Include header file */
#include "ina219.h"

/* Extern i2c data struct from main */
extern I2C_HandleTypeDef hi2c1;

/* Read shunt voltage */
float INA219_Read_Shunt_Voltage(void)
{
	int16_t Shunt_Voltage_Data = 0;
	uint8_t INA219_Shunt_Voltage_Register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Shunt_Voltage, 1, &INA219_Shunt_Voltage_Register[0], 2, 4);
	Shunt_Voltage_Data = ((INA219_Shunt_Voltage_Register[0]<<8) | INA219_Shunt_Voltage_Register[1]);
	return Shunt_Voltage_Data * 0.01f;
}

/* Read bus voltage */
float INA219_Read_Bus_Voltage(void)
{
	int16_t Bus_Voltage_Data = 0;
	uint8_t INA219_Bus_Voltage_Register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Bus_Voltage, 1, &INA219_Bus_Voltage_Register[0], 2, 4);
	if(INA219_Bus_Voltage_Register[1] & 0x01)
	{
		return -1;
	}
	else
	{
		if(INA219_Bus_Voltage_Register[1] & 0x02)
		{
			Bus_Voltage_Data = ((INA219_Bus_Voltage_Register[0] << 5) | (INA219_Bus_Voltage_Register[1] >> 3));
			return Bus_Voltage_Data * 4.0f;
		}
		return 0;
	}
}

/* Read calculated bus power */
float INA219_Read_Bus_Power(void)
{
	int16_t Power_Data = 0;
	uint8_t INA219_Power_Register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Power, 1, &INA219_Power_Register[0], 2, 4);
	Power_Data = ((INA219_Power_Register[0]<<8) | INA219_Power_Register[1]);
	// 0.0018310546875 is Power_LSB that equal to 2 * Current LSB, Watt
	return (Power_Data  * 0.0018310546875f) * 1000.0f; // miliWatt
}

/* Read bus current */
float INA219_Read_Bus_Current(void)
{
    int16_t Current_Data = 0;
	uint8_t INA219_Current_Register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Current, 1, &INA219_Current_Register[0], 2, 4);
	Current_Data = ((INA219_Current_Register[0]<<8) | INA219_Current_Register[1]);
	// 0.000091552734375 is Current_LSB that equal to max expected current (3A) / 2^15, Ampere
	return Current_Data  * 0.000091552734375f;
}

/* Sensor initialization */
void INA219_Init(void)
{
    uint8_t INA219_Config[3] = {INA219_Configuration, INA219_vConfiguration >> 8, (uint8_t) INA219_vConfiguration};
    uint8_t INA219_Calib[3] = {INA219_Calibration, INA219_vCalibration >> 8, (uint8_t) INA219_vCalibration};
    uint8_t INA219_Config_Ver[2] = {0};
    uint8_t INA219_Calib_Ver[2] = {0};
    HAL_I2C_Master_Transmit(&hi2c1, INA219_Address, &INA219_Config[0], 3, 3);
    HAL_I2C_Master_Transmit(&hi2c1, INA219_Address, &INA219_Calib[0], 3, 3);
    HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Configuration, 1, &INA219_Config_Ver[0], 2, 4);
    HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Calibration, 1, &INA219_Calib_Ver[0], 2, 4);
}
