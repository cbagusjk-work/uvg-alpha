/* Include header file */
#include "sim5360.h"

/* Extern UART data struct and error flag from main */
extern 	UART_HandleTypeDef huart2;
extern	uint8_t SIM5360_Init_Error_Flag;

/* Required variable */
char	NMEA_Buffer[85] = {0};
char 	NMEA_Sentence[7] = {0};
_Bool 	NMEA_Char_Side = 0;
_Bool 	NMEA_RMC = 0;
_Bool	NMEA_LF = 0;
uint8_t NMEA_Char_Count = 0;
uint8_t	GP = 1;
uint8_t	GN = 1;
uint8_t	AT_Command[64] = {0};
uint8_t	AT_Reply[256] = {0};
uint8_t Batt_Voltage[6] = {0};

/* Initialize SIM5360 */
void SIM5360A_Init(void)
{
	SIM5360A_Check_Power();
	SIM5360A_Echo_Off();
	SIM5360A_Reroute_NMEA_Output();
	SIM5360A_RMC_Selection();
	SIM5360A_Automate_Activating_GPS();
	SIM5360A_Activate_GPS();
	SIM5360A_Check_Network_Registration();
	SIM5360A_Check_PDP_Context();
	SIM5360A_Open_Network();
	SIM5360A_Open_UDP_Connection();
	SIM5360A_Set_AGPS();
	SIM5360A_Set_LP_Warning();
	SIM5360A_Save_Settings();
}

/* Check SIM5360 status */
void SIM5360A_Check_Power(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
    SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT\r\n\0", 10, 100);
		if(strstr((char*) &AT_Reply[0], "OK\r\n") == NULL)
		{
      HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
			if(!STM32F103_Check_Batt_Volt())
			{
        STM32F103_Sleep_Minutes(1);
				STM32F103_Enter_Standby();
			}
			if(Try > 2)
			{
				Try = 0;
				SIM5360_Init_Error_Flag |= 0x01;
			}
			SIM5360A_Power_State_Changer(1);
			HAL_Delay(15000);
		}
		else
		{
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			SIM5360_Init_Error_Flag &= ~(0x01);
			break;
		}
	}
}

/* Turn off ECHO */
void SIM5360A_Echo_Off(void)
{
	SIM5360A_Clear_AT_Buffer(64, 256);
	SIM5360A_SAR_AT_Command("ATE0\r\n\0", 6, 100);
}

/* Reroute NMEA sentence output to specific UART and baudrate */
void SIM5360A_Reroute_NMEA_Output(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CGPSSWITCH?\r\n\0", 31, 500);
		if(strstr((char*) &AT_Reply[0], "+CGPSSWITCH: 3,115200\r\n") == NULL)
		{
			if(Try > 3)
			{
				Try = 0;
				SIM5360_Init_Error_Flag |= 0x02;
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				continue;
			}
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
			SIM5360A_Clear_AT_Buffer(64, 256);
			SIM5360A_SAR_AT_Command("AT+CGPSSWITCH=3,115200\r\n\0", 6, 500);
			SIM5360A_Power_State_Changer(0);
			HAL_Delay(10000);
			SIM5360A_Power_State_Changer(1);
			HAL_Delay(15000);
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
			SIM5360A_Echo_Off();
		}
		else
		{
			SIM5360_Init_Error_Flag &= ~(0x02);
			break;
		}
	}
}

/* Outputing only NMEA RMC sentence */
void SIM5360A_RMC_Selection(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CGPSNMEA?\r\n\0", 22, 500);
		if(strstr((char*) &AT_Reply[0], "+CGPSNMEA: 2\r\n") == NULL)
		{
			if(Try > 3)
			{
				SIM5360_Init_Error_Flag |= 0x04;
				break;
			}
			SIM5360A_SAR_AT_Command("AT+CGPSNMEA=2\r\n\0", 9, 500);
			if(strstr((char*) &AT_Reply[0], "ERROR") != NULL)
			{
				SIM5360A_SAR_AT_Command("AT+CGPS=0\r\n\0", 6, 500);
				HAL_Delay(2000);
			}
		}
		else
		{
			SIM5360_Init_Error_Flag &= ~(0x04);
			break;
		}
	}
}

/* Automatic activate GPS */
void SIM5360A_Automate_Activating_GPS(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CGPSAUTO?\r\n\0", 22, 500);
		if(strstr((char*) &AT_Reply[0], "+CGPSAUTO: 1\r\n") == NULL)
		{
			if(Try > 3)
			{
				SIM5360_Init_Error_Flag |= 0x08;
				break;
			}
			SIM5360A_SAR_AT_Command("AT+CGPSAUTO=1\r\n\0", 6, 500);
		}
		else
		{
			SIM5360_Init_Error_Flag &= ~(0x08);
			break;
		}
	}
}

/* Activate GPS */
void SIM5360A_Activate_GPS(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CGPS?\r\n\0", 20, 500);
		if(strstr((char*) &AT_Reply[0], "+CGPS: 1,1\r\n") == NULL)
		{
			if(Try > 3)
			{
				SIM5360_Init_Error_Flag |= 0x10;
				break;
			}
			SIM5360A_SAR_AT_Command("AT+CGPS=1\r\n\0", 6, 500);
		}
		else
		{
			SIM5360_Init_Error_Flag &= ~(0x10);
			break;
		}
	}
}

/* Check network registration to network provider (Telkomsel, Indosat, etc)*/
void SIM5360A_Check_Network_Registration(void)
{
	uint8_t Try = 0;
	SIM5360A_Clear_AT_Buffer(64, 256);
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CREG?\r\n\0", 20, 100);
		if(strstr((char*) &AT_Reply[0], "+CREG: 0,1\r\n") == NULL)
		{
			if(Try > 100)
			{
				Try = 0;
				HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
				SIM5360A_SAR_AT_Command("AT+COPS=2\r\n\0", 8, 10);
				HAL_Delay(5000);
				SIM5360A_SAR_AT_Command("AT+COPS=0\r\n\0", 8, 10);
				HAL_Delay(5000);
				HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				SIM5360A_Reroute_NMEA_Output();
				SIM5360A_RMC_Selection();
				SIM5360A_Automate_Activating_GPS();
				SIM5360A_Activate_GPS();
			}
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			HAL_Delay(400);
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
			HAL_Delay(100);
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			HAL_Delay(400);
		}
		else
		{
			break;
		}
	}
}

/* Check Packet Data Protocol, required for internet access */
void SIM5360A_Check_PDP_Context(void)
{
	SIM5360A_Clear_AT_Buffer(64, 256);
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(30, 6);
		SIM5360A_SAR_AT_Command("AT+CGDCONT?\r\n\0", 51, 300);
		if(strstr((char*) &AT_Reply[0], "+CGDCONT: 1,\"IP\",\"internet\"") == NULL)
		{
			if(Try > 3)
			{
				Try = 0;
				HAL_Delay(2000);
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				SIM5360A_Reroute_NMEA_Output();
				SIM5360A_RMC_Selection();
				SIM5360A_Automate_Activating_GPS();
				SIM5360A_Activate_GPS();
				SIM5360A_Check_Network_Registration();
				continue;
			}
			SIM5360A_Clear_AT_Buffer(13, 51);
			SIM5360A_SAR_AT_Command("AT+CGDCONT=1,\"IP\",\"internet\"\r\n\0", 6, 300);
		}
		else
		{
			break;
		}
	}
}
void SIM5360A_Check_PDS_Status(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+CGATT?\r\n\0", 19, 300);
		if(strstr((char*) &AT_Reply[0], "+CGATT: 1\r\n") == NULL)
		{
			if(Try > 3)
			{
				Try = 0;
				HAL_Delay(2000);
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				SIM5360A_Reroute_NMEA_Output();
				SIM5360A_RMC_Selection();
				SIM5360A_Automate_Activating_GPS();
				SIM5360A_Activate_GPS();
				SIM5360A_Check_Network_Registration();
				SIM5360A_Check_PDP_Context();
				continue;
			}
			SIM5360A_SAR_AT_Command("AT+CGATT=1\r\n\0", 6, 100);
			HAL_Delay(3000);
		}
		else
		{
			break;
		}
	}
}
void SIM5360A_Open_Network(void)
{
	uint8_t Try = 0;
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(64, 256);
		SIM5360A_SAR_AT_Command("AT+NETOPEN?\r\n\0", 23, 1500);
		if(strstr((char*) &AT_Reply[0], "+NETOPEN: 1,0\r\n") == NULL)
		{
			if(Try > 3)
			{
				Try = 0;
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				SIM5360A_Reroute_NMEA_Output();
				SIM5360A_RMC_Selection();
				SIM5360A_Automate_Activating_GPS();
				SIM5360A_Activate_GPS();
				SIM5360A_Check_Network_Registration();
				SIM5360A_Check_PDP_Context();
				//SIM5360A_Check_PDS_Status();
				continue;
			}
			SIM5360A_Clear_AT_Buffer(13, 23);
			SIM5360A_SAR_AT_Command("AT+NETOPEN\r\n\0", 42, 300);
			HAL_Delay(5000);
		}
		else
		{
			break;
		}
	}
}
void SIM5360A_Open_UDP_Connection(void)
{
	uint8_t Try = 0;
	SIM5360A_Clear_AT_Buffer(64, 128);
	while(1)
	{
		Try++;
		SIM5360A_Clear_AT_Buffer(25, 165);
		SIM5360A_SAR_AT_Command("AT+CIPOPEN?\r\n\0", 176, 150);
		if(strstr((char*) &AT_Reply[0], "+CIPOPEN: 0,\"UDP\",") == NULL)
		{
			if(Try > 2)
			{
        Try = 0;
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
				SIM5360A_Check_Power();
				SIM5360A_Echo_Off();
				SIM5360A_Reroute_NMEA_Output();
				SIM5360A_RMC_Selection();
				SIM5360A_Automate_Activating_GPS();
				SIM5360A_Activate_GPS();
				SIM5360A_Check_Network_Registration();
				SIM5360A_Check_PDP_Context();
				//SIM5360A_Check_PDS_Status();
				SIM5360A_Open_Network();
				SIM5360A_Set_AGPS();
				SIM5360A_Set_LP_Warning();
				SIM5360A_Save_Settings();
				continue;
			}
			SIM5360A_Clear_AT_Buffer(13, 147);
			SIM5360A_SAR_AT_Command("AT+CIPOPEN=0,\"UDP\",,,42680\r\n\0", 23, 23);
			HAL_Delay(1000);
		}
		else
		{
			break;
		}
	}
}
void SIM5360A_Set_AGPS(void)
{
	SIM5360A_SAR_AT_Command("AT+CGPSURL=\"SUPL.GOOGLE.COM\"\r\n\0", 10, 20);
	SIM5360A_SAR_AT_Command("AT+CGPSXE=1\r\n\0", 10, 20);
	SIM5360A_SAR_AT_Command("AT+CGPSXD=0\r\n\0", 10, 20);
	SIM5360A_SAR_AT_Command("AT+CGPSXDAUTO=1\r\n\0", 10, 20);
}

/* Set Low Voltage threshold */
void SIM5360A_Set_LP_Warning(void)
{
	SIM5360A_SAR_AT_Command("AT+CVALARM=1,3400,4200\r\n\0", 6, 150);
	SIM5360A_SAR_AT_Command("AT+CPMVT=1,3300,4300\r\n\0", 6, 150);
}
void SIM5360A_Save_Settings(void)
{
	SIM5360A_Clear_AT_Buffer(64, 256);
	SIM5360A_SAR_AT_Command("AT&W0\r\n\0", 100, 200);
}
uint8_t SIM5360A_NMEA_Handler(char NMEA_Char)
{
	/* Store current arrived char */
	char Current_Char = NMEA_Char;

	/* Determine current arrived char is the first, middle, or last of NMEA sentence */
	switch (Current_Char)
	{
		/* First of NMEA sentence */
		case '$' :
			NMEA_Char_Side	= 1;
			NMEA_RMC				= 0;
			NMEA_LF					= 0;
			NMEA_Char_Count = 0;
			break;

		/* Last of NMEA sentence */
		case '\r':
			NMEA_Char_Side	= 0;
			if(NMEA_RMC)
			{
				NMEA_LF     	= 1;
			}
			break;

		/* Something else */
		default:
			break;
	}

	/* First NMEA sentence detected */
	if(NMEA_Char_Side)
	{
		/* Store 6 char after */
		if(NMEA_Char_Count <= 5)
		{
			NMEA_Sentence[NMEA_Char_Count] = Current_Char;
		}

		/* Hasbeen store 6 chars */
		if(NMEA_Char_Count == 5)
		{
			/* Search for NMEA RMC sentence */
			GP = strcmp(NMEA_Sentence, "$GPRMC");
			GN = strcmp(NMEA_Sentence, "$GNRMC");

			/* NMEA RMC sentence present */
			if(GP == 0 || GN == 0)
			{
				/* Copy to buffer */
				NMEA_RMC = 1;
				memcpy(&NMEA_Buffer[0], &NMEA_Sentence[0], 6);
			}
      else
      {
				/* Reset stat */
        NMEA_Char_Side	= 0;
        NMEA_Char_Count = 0;
        return 2;
      }
			GP = 1;
			GN = 1;
		}

		/* NMEA RMC sentence present */
		if(NMEA_RMC)
		{
			/* Store current char to buffer */
			NMEA_Buffer[NMEA_Char_Count] = Current_Char;
		}

		/* Increment element counter */
		NMEA_Char_Count++;
	}

	/* Last char of NMEA sentence present */
	if(NMEA_LF == 1 && NMEA_Char_Count > 63)
	{
		STM32F103_NMEA_Parser((char*) &NMEA_Buffer[0]);
		return 1;
	}
	else if(NMEA_LF == 1 && NMEA_Char_Count < 64)
	{
        memset(&NMEA_Buffer[0], 0x00, 85);
		return 0;
	}
	else
	{
		return 2;
	}
}
void SIM5360A_Clear_AT_Buffer(uint8_t Size_Command_Buffer, uint16_t Size_Reply_Buffer)
{
	memset(&AT_Command[0], 0x00, Size_Command_Buffer);
	memset(&AT_Reply[0], 0x00, Size_Reply_Buffer);
}
void SIM5360A_SAR_AT_Command(const char* AT_Command_Data, uint8_t Size_Reply, uint16_t Timeout)
{
	uint8_t AT_Command_Size = strlen(AT_Command_Data);
	memcpy(&AT_Command[0], &AT_Command_Data[0], AT_Command_Size);
	HAL_UART_Transmit(&huart2, &AT_Command[0], AT_Command_Size, AT_Command_Size);
	HAL_UART_Receive(&huart2, &AT_Reply[0], Size_Reply, Timeout);
}
void SIM5360A_Power_State_Changer(_Bool State)
{
	HAL_GPIO_WritePin(PWRSW_GPIO_Port, PWRSW_Pin, GPIO_PIN_SET);
	if(State)
	{
		HAL_Delay(2000);
	}
	else
	{
		HAL_Delay(2300);
	}
	HAL_GPIO_WritePin(PWRSW_GPIO_Port, PWRSW_Pin, GPIO_PIN_RESET);
}
void SIM5360A_Turn_Off(void)
{
  SIM5360A_Clear_AT_Buffer(64, 256);
  uint8_t Try = 0;
  while(Try < 3)
  {
    Try++;
    SIM5360A_SAR_AT_Command("AT\r\n\0", 10, 100);
    if(strstr((char*) &AT_Reply[0], "OK") != NULL)
    {
      SIM5360A_Power_State_Changer(0);
      break;
    }
    HAL_Delay(2000);
  }
}
