/* Include header file */
#include "lsm6ds3.h"

/* Extern spi data struct from main */
extern 		SPI_HandleTypeDef hspi2;

/* Required variable */
uint8_t		LSM6DS3_READ_XL 	= OUTX_L_XL | 0x80;
uint8_t		Sensor_Value[6]		= {0};

/* IMU initialize */
void LSM6DS3_Init(void)
{
	uint8_t Data_Registers[26] =
	{
		FUNC_CFG_ACCESS, vFUNC_CFG_ACCESS,		// Enable access to embedded function configuration register
		SM_THS, vSM_THS,											// Write significant motion threshold
		PEDO_THS_REG, vPEDO_THS_REG,					// Set PEDO_4G and min threshold
		FUNC_CFG_ACCESS, vFUNC_CFG_ACCESS2,  	// Disable access to embedded function configuration register
		CTRL1_XL, vCTRL1_XL,									// Configuration for accelerometer
		CTRL5_C, vCTRL5_C,										// Configuration for register address auto increament
		CTRL8_XL, vCTRL8_XL,									// LPF2 for accelerometer
		CTRL4_C, vCTRL4_C,										// Enable data ready mask and I2C disabled
		CTRL10_C, vCTRL10_C, 									// Enable embedded function (tilt and significant motion) and enable significant motion
		TAP_CFG, vTAP_CFG,										// Enable pedometer for step detection in order to help significant motion works properly
		INT1_CTRL, vINT1_CTRL,								// Configuration for INT1 pin / significant motion
		MD1_CFG, vMD1_CFG,										// Configuration for tilt detection
		INT2_CTRL, vINT2_CTRL 								// Configuration for INT2 pin / data ready
	};
	for(uint8_t Index = 0; Index < 26; Index += 2)
	{
		HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_RESET);
		HAL_SPI_Transmit(&hspi2, &Data_Registers[Index], 2, 2);
    HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_SET);
	}
}

/* Power down IMU */
void LSM6DS3_Power_Down()
{
    uint8_t Data_Registers[2] =
	{
		CTRL1_XL, lCTRL1_XL
	};
    HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi2, &Data_Registers[0], 2, 2);
    HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_SET);
}

/* Accelerometer read */
void LSM6DS3_Read(uint16_t Sensor_Output[])
{
	uint8_t Index2 = 0;
	HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi2, &LSM6DS3_READ_XL, 1, 1);
	HAL_SPI_Receive(&hspi2, &Sensor_Value[0], 6, 6);
	HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_SET);
	// combine 8 bit L and H to 16 bit
	/** combinedSensor array element :
	  * 0 = accel X
	  * 1 = accel Y
	  * 2 = accel Z
	  */
	for(uint8_t Index = 0; Index < 3; Index++)
	{
		Sensor_Output[Index]  = Sensor_Value[Index2];
		Sensor_Output[Index] |= (Sensor_Value[++Index2]<<8);
		Index2++;
	}
}

/* Check interrupt source */
uint8_t LSM6DS3_Check_Interrupt(void)
{
	uint8_t Interrupt_Source[2] = {(FUNC_SRC | 0x80), 0x00};   // 0x80 for Read operation
	HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi2, &Interrupt_Source[0], 1, 2);
	HAL_SPI_Receive(&hspi2, &Interrupt_Source[1], 1, 2);
	HAL_GPIO_WritePin(IMU_SS_GPIO_Port, IMU_SS_Pin, GPIO_PIN_SET);
	if(Interrupt_Source[1] & 0x40)
	{
		/* Significant motion */
		return 0x04;
	}
	else if(Interrupt_Source[1] & 0x20)
	{
		/* Tilt detection */
		return 0x08;
	}
	else
	{
		/* Something Else */
		return 0x02;
	}
}
